package com.training.springdemo;

import org.springframework.stereotype.Component;

@Component
public class Pet {
    public void feed() {
        System.out.println("Pet is being fed");
    }
}
