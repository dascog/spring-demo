package com.training.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Person {

    @Autowired
    private Pet pet;

    public Person() {
        System.out.println("Person is created");
    }
    public void sayHello() {
        System.out.println("Hello!");
    }

    public void feedPet() {
        System.out.println("Person is feeding the pet");
        pet.feed();
    }
}